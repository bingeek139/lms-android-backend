package com.ptit.lms.androidlms.dto;

import com.ptit.lms.androidlms.entity.Exam;
import com.ptit.lms.androidlms.entity.UserClazz;
import lombok.Data;

import java.util.List;
@Data
public class ClazzDto {
    private String id;
    private String name;
    private String code;
    private String description;
    private Integer status;
}
