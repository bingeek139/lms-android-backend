package com.ptit.lms.androidlms.common.helper;

import lombok.Data;

@Data
public class ApiResponse {
    private int code;
    private Boolean status;
    private String errors;
    private Object data;
    private Integer page;
    private Integer pageSize;
    private Integer totalRow;

    public ApiResponse(int code, String errors, Object data) {
        super();
        this.code = code;
        this.errors = errors;
        this.data = data;
    }

    public ApiResponse(int code, String errors) {
        super();
        this.code = code;
        this.errors = errors;
    }

    public ApiResponse() {
        super();
    }
}