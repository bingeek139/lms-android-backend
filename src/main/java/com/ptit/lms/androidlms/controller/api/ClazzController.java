package com.ptit.lms.androidlms.controller.api;

import com.ptit.lms.androidlms.common.util.Const;
import com.ptit.lms.androidlms.common.util.GenericMapper;
import com.ptit.lms.androidlms.common.util.ResponseCodeEnum;
import com.ptit.lms.androidlms.controller.request.ClazzRequest;
import com.ptit.lms.androidlms.controller.response.ResponseBodyDto;
import com.ptit.lms.androidlms.dto.ClazzDto;
import com.ptit.lms.androidlms.entity.Clazz;
import com.ptit.lms.androidlms.entity.ResultDetail;
import com.ptit.lms.androidlms.entity.UserClazz;
import com.ptit.lms.androidlms.repository.ClazzRepository;
import com.ptit.lms.androidlms.repository.UserClazzRepository;
import com.ptit.lms.androidlms.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@RestController("/v1/class")
public class ClazzController {
    @Autowired
    ClazzRepository clazzRepository;

    @Autowired
    GenericMapper genericMapper;

    @PostMapping
    public ResponseEntity<ResponseBodyDto> create(@RequestBody @Valid ClazzRequest.Create clazzRequest){
        Clazz clazz = genericMapper.mapToType(clazzRequest, Clazz.class);
        clazz.setCode(Long.toHexString(Instant.now().getEpochSecond()));
        clazz.setStatus(Const.ACTIVE);
        ClazzDto clazzDto = genericMapper.mapToType(clazzRepository.save(clazz), ClazzDto.class);
        var responseBodyDto=ResponseBodyDto.ofCreated("Tạo lớp thành công",clazzDto);
        return ResponseEntity.ok(responseBodyDto);
    }

    @Autowired
    UserClazzRepository userClazzRepository;

    @Autowired
    UserRepository userRepository;
    @GetMapping("join/{code}")
    public ResponseEntity<ResponseBodyDto> join(@PathVariable("code")String code){
        List<Clazz> clazzes = clazzRepository.findByCode(code);
        ResponseBodyDto responseBodyDto;
        if(clazzes.size() > 0){
           Clazz clazz= clazzes.get(0);
            Authentication authentication=SecurityContextHolder.getContext().getAuthentication();
            if(!Objects.isNull(authentication)){
                org.springframework.security.core.userdetails.User user = (User) authentication.getPrincipal();
                Optional<com.ptit.lms.androidlms.entity.User> optionalUser = userRepository.findByUsername(user.getUsername());
                UserClazz userClazz=new UserClazz();
                userClazz.setClazz(clazz);
                userClazz.setUser(optionalUser.orElseGet(null));
                userClazzRepository.save(userClazz);
                responseBodyDto=ResponseBodyDto.ofSuccess(genericMapper.mapToType(clazz,ClazzDto.class));
            }else {
                responseBodyDto=new ResponseBodyDto(ResponseCodeEnum.R_500.name(),"Vui lòng đăng nhập",null);
            }

        }else {
            responseBodyDto=new ResponseBodyDto(ResponseCodeEnum.R_404.name(),"Mã lớp không tồn tại",null);
        }
        return ResponseEntity.ok(responseBodyDto);
    }
}