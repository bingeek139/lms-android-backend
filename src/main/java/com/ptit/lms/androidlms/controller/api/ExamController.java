package com.ptit.lms.androidlms.controller.api;

import com.ptit.lms.androidlms.common.util.GenericMapper;
import com.ptit.lms.androidlms.common.util.ResponseCodeEnum;
import com.ptit.lms.androidlms.controller.request.Submit;
import com.ptit.lms.androidlms.controller.response.ResponseBodyDto;
import com.ptit.lms.androidlms.dto.AnswerDto;
import com.ptit.lms.androidlms.dto.ExamDto;
import com.ptit.lms.androidlms.dto.QuestionDto;
import com.ptit.lms.androidlms.dto.ResultSubmitDto;
import com.ptit.lms.androidlms.entity.*;
import com.ptit.lms.androidlms.repository.*;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@RequestMapping("/v1/exam")
@RestController
public class ExamController {
    @Autowired
    ExamRepository examRepository;

    @Autowired
    GenericMapper genericMapper;

    @Autowired
    ClazzRepository clazzRepository;

    @Autowired
    ResultRepository resultRepository;

    @Autowired
    ResultDetailRepository resultDetailRepository;

    @Transactional
    @GetMapping("{id}")
    public ResponseEntity<ResponseBodyDto> getId(@PathVariable("id") String id) throws Exception {
        Optional<Exam> optionalExam = examRepository.findById(id);
        ResponseBodyDto responseBodyDto;
        if (optionalExam.isPresent()) {
            Exam exam = optionalExam.get();
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!Objects.isNull(authentication)) {
                ExamDto examDto = genericMapper.mapToTypeNotNullProperty(exam, ExamDto.class);
                org.springframework.security.core.userdetails.User user = (User) authentication.getPrincipal();
                Optional<com.ptit.lms.androidlms.entity.User> optionalUser = userRepository.findByUsername(user.getUsername());
                if (optionalUser.isPresent()) {
                    var currentUser = optionalUser.get();
                    Result result = new Result();
                    result.setExamId(id);
                    result.setUser(currentUser);
                    result.setDeadline(Instant.now().plusSeconds(exam.getTimeLimit() + 2));
                    Result resultSave = resultRepository.save(result);
                    List<Question> questions = exam.getQuestions();
                    List<QuestionDto> questionDtos = new ArrayList<>();
                    for (Question question : questions) {
                        QuestionDto questionDto = new QuestionDto();
                        genericMapper.mapSrcToDestNotNullProperty(question, questionDto);
                        List<Answer> answers = question.getAnswers();
                        List<AnswerDto> answerDtos = genericMapper.mapToListOfTypeNotNullProperty(answers, AnswerDto.class);
                        List<Integer> order = genOrder(answerDtos.size());

                        for (int i = 0; i < answerDtos.size(); i++) {
                            answerDtos.get(i).setOrdinal(order.get(i));
                            ResultDetail resultDetail = new ResultDetail();
                            resultDetail.setAnswer(answers.get(i));
                            resultDetail.setIsPick(false);
                            resultDetail.setResult(resultSave);
                            resultDetail.setOrdinal(order.get(i));
                            resultDetailRepository.save(resultDetail);
                        }
                        questionDto.setAnswerDtos(answerDtos);
                        questionDtos.add(questionDto);
                    }

                    examDto.setQuestionDtoList(questionDtos);
                    responseBodyDto = new ResponseBodyDto(ResponseCodeEnum.R_200.name(), "Thành công", examDto);
                } else {
                    throw new Exception("Vui lòng đăng nhập");
                }
            } else {
                throw new Exception("Token không tồn tại");
            }

        } else {
            responseBodyDto = new ResponseBodyDto(ResponseCodeEnum.R_404.name(), "Bài kiểm tra không tồn tại", ExamDto.class);
        }
        return ResponseEntity.ok(responseBodyDto);
    }

    private List<Integer> genOrder(int size) {
        Set<Integer> integerSet = new LinkedHashSet<Integer>();
        Random rng = new Random();
        while (integerSet.size() < size) {
            integerSet.add(rng.nextInt(size) + 1);
        }
        List<Integer> integers = integerSet.stream().collect(Collectors.toList());
        return integers;
    }

    @Transactional
    @PostMapping("submit/{idResult}")
    public ResponseEntity<ResponseBodyDto> submit(@RequestBody Submit submit) throws Exception {
        Optional<Result> optionalResult = resultRepository.findById(submit.getResultId());

        Map<String,Boolean> map=new HashMap<>();
        if(!optionalResult.isPresent()){
            throw new Exception("Mã Bài thi không tồn tại");
        }
        if(Instant.now().compareTo(optionalResult.get().getDeadline()) > 0){
            throw new Exception("Đã hết thời gian nộp bài");
        }
        int quantityCorrect=0;
        List<ResultDetail> resultDetailByResult = resultDetailRepository.findResultDetailByResult(optionalResult.get());
         for (String s : submit.getIdAnswer()) {
                Boolean isTrue=Boolean.FALSE;
                for (ResultDetail resultDetail : resultDetailByResult) {
                    if(resultDetail.getResult().getExamId().equals(s)){
                        resultDetail.setIsPick(Boolean.TRUE);
                        if(resultDetail.getAnswer().getIsTrue().equals(1)){
                            isTrue=Boolean.TRUE;
                            quantityCorrect++;
                        }
                        break;
                    }
                 }
                map.put(s,isTrue);
             }
        ResultSubmitDto resultSubmitDto=new ResultSubmitDto();
         resultSubmitDto.setResults(map);
        Optional<Exam> byId = examRepository.findById(optionalResult.get().getExamId());
        Integer quantity=byId.get().getQuantity();
        resultSubmitDto.setQuantityCorrect(quantity);
        resultSubmitDto.setQuantityCorrect(quantityCorrect);
        ResponseBodyDto responseBodyDto = ResponseBodyDto.ofSuccess(resultSubmitDto);
        return ResponseEntity.ok(responseBodyDto);


    }

    @Autowired
    UserRepository userRepository;

    @GetMapping("class/{id}")
    public ResponseEntity<ResponseBodyDto> getByClassId(@PathVariable("id") String idClazz) {
        Optional<Clazz> optionalClazz = clazzRepository.findByIdAndAndFetchExamsEagerly(idClazz);
        ResponseBodyDto responseBodyDto;
        if (optionalClazz.isPresent()) {
            responseBodyDto = ResponseBodyDto.ofSuccess(genericMapper.mapToListOfType(optionalClazz.get().getExams(), Exam.class));
        } else {
            responseBodyDto = new ResponseBodyDto(ResponseCodeEnum.R_404.name(), "Mã lớp không tồn tại", null);
        }
        return ResponseEntity.ok(responseBodyDto);
    }


}
