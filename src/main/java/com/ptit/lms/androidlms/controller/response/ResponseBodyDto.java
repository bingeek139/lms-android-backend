package com.ptit.lms.androidlms.controller.response;

import com.ptit.lms.androidlms.common.util.ResponseCodeEnum;
import org.springframework.web.bind.annotation.ResponseBody;

public class ResponseBodyDto {
    private String code; //Constants.ERROR_CODE.
    private String message;
    private Object data;

    public ResponseBodyDto() {
    }

    public ResponseBodyDto(String errorCode) {
        this.code = errorCode;
    }

    public ResponseBodyDto(String errorCode, String message) {
        this.code = errorCode;
        this.message = message;
    }

    public ResponseBodyDto(String errorCode, String message, Object data) {
        this.code = errorCode;
        this.message = message;
        this.data = data;
    }

    public static ResponseBodyDto ofCreated( String message,Object data){
        ResponseBodyDto responseData = new ResponseBodyDto();
        responseData.setCode(ResponseCodeEnum.R_201);
        responseData.setData(data);
        responseData.setMessage(message);
        return responseData;

    }

    public static ResponseBodyDto ofSuccess( Object data){
        ResponseBodyDto responseData = new ResponseBodyDto();
        responseData.setCode(ResponseCodeEnum.R_200);
        responseData.setData(data);
        responseData.setMessage("Thành công");
        return responseData;

    }
//
//    public static ResponseBody ofSuccess() {
//        ResponseBody responseData = new ResponseBody();
//        responseData.setErrorCode(Constants.ERROR_CODE.SUCCESS);
//        return responseData;
//    }
//
//    public static ResponseBody ofSuccess(String message) {
//        ResponseBody responseData = new ResponseBody();
//        responseData.setMessage(message);
//        responseData.setErrorCode(Constants.ERROR_CODE.SUCCESS);
//        return responseData;
//    }
//
//    public static ResponseBody ofSuccess(String message, Object data) {
//        ResponseBody responseData = new ResponseBody();
//        responseData.setMessage(message);
//        responseData.setData(data);
//        responseData.setErrorCode(Constants.ERROR_CODE.SUCCESS);
//        return responseData;
//    }
//
//    public static ResponseBody ofFail(String message) {
//        ResponseBody responseData = new ResponseBody();
//        responseData.setMessage(message);
//        responseData.setErrorCode(Constants.ERROR_CODE.VALIDATE_FAIL);
//        return responseData;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(ResponseCodeEnum code) {
        this.code = code.name();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}