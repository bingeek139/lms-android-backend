package com.ptit.lms.androidlms.controller.request;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

public class ClazzRequest {
    @Data
    @Accessors(chain = true)
    public static class Create{
        @NotNull(message = "Tên không được để trống")
        private String name;
        private String description;
    }
}
