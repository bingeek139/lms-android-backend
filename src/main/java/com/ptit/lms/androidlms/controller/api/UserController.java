package com.ptit.lms.androidlms.controller.api;

import com.ptit.lms.androidlms.common.helper.ApiResponse;
import com.ptit.lms.androidlms.entity.User;
import com.ptit.lms.androidlms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("user")
@CrossOrigin(origins = {"*"})
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<ApiResponse> addUser(@RequestBody User user) {
        boolean flag = userService.addUser(user);
        ApiResponse object = new ApiResponse();
        if (!flag) {
            object.setCode(409);
            object.setErrors("User is exists!");
            object.setStatus(false);
            return new ResponseEntity<>(object, HttpStatus.OK);
        }

        object.setCode(200);
        object.setErrors(null);
        object.setStatus(true);
        return new ResponseEntity<>(object, HttpStatus.OK);
    }

    @PutMapping("update")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        userService.updateUser(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping("deleteByName/{username}")
    public ResponseEntity<Void> deleteUserByUsername(@PathVariable("username") String userName) {
        userService.deleteUserByUsername(userName);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("deleteById/{id}")
    public ResponseEntity<Void> deleteUserById(@PathVariable("id") Integer id) {
        userService.deleteUserById(id);
//		userService.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}