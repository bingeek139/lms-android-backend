package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LevelRepository extends JpaRepository<Level,String> {
}
