package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionRepository extends JpaRepository<Question,String> {
}
