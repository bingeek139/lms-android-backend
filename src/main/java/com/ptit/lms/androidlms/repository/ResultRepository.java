package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.Result;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResultRepository extends JpaRepository<Result,String> {
}
