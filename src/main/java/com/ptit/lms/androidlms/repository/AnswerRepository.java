package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnswerRepository extends JpaRepository<Answer,String> {
}
