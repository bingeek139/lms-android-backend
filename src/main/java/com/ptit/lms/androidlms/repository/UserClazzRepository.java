package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.User;
import com.ptit.lms.androidlms.entity.UserClazz;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserClazzRepository extends JpaRepository<UserClazz,String> {
}
