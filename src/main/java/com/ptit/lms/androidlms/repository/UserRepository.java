package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User,String> {
    Optional<User> findByUsername(String userName);
}
