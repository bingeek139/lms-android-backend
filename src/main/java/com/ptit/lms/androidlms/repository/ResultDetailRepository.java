package com.ptit.lms.androidlms.repository;

import com.ptit.lms.androidlms.entity.Result;
import com.ptit.lms.androidlms.entity.ResultDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ResultDetailRepository extends JpaRepository<ResultDetail,String> {
    List<ResultDetail> findResultDetailByResult(Result result);
}
