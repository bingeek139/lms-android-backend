package com.ptit.lms.androidlms.dao;

import com.ptit.lms.androidlms.dto.TokenResetPassDTO;
import com.ptit.lms.androidlms.entity.User;

public interface UserDao {

    boolean authenUser(String username, String password);

    User getUserByUserName(String userName);

    boolean addUser(User user);

    void updateUser(User user);

    void deleteUserByUsername(String user_name);

    boolean userExists(User user);

    void deleteUserById(Integer id);

    boolean changePassword(TokenResetPassDTO tokenResetDTO);
}