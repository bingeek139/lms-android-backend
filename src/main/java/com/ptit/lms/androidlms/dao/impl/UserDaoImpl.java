package com.ptit.lms.androidlms.dao.impl;

import com.ptit.lms.androidlms.dao.UserDao;
import com.ptit.lms.androidlms.dto.ApiHelper;
import com.ptit.lms.androidlms.dto.EmailHelper;
import com.ptit.lms.androidlms.dto.TokenResetPassDTO;
import com.ptit.lms.androidlms.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private ApiHelper apiHelper;

	@Autowired
	private EmailHelper emailHelper;

	@Autowired
	private PasswordEncoder passwordEncoder;


	@SuppressWarnings("unchecked")
	@Override
	public boolean authenUser(String username, String password) {
		String hql = "FROM User AS u WHERE u.username = :username AND u.password = :password AND u.activeFlg = 1 AND u.isDeleted = 0";
		List<User> lstUsers = (List<User>) entityManager.createQuery(hql).setParameter("username", username).setParameter("password", password).getResultList();
		return lstUsers != null && lstUsers.size() > 0;
	}

	@Override
	public User getUserByUserName(String userName) {
		String hql = "FROM User AS u WHERE u.username = :username AND u.isDeleted=0 AND u.activeFlg = 1";
		List<User> lstResult = entityManager.createQuery(hql).setParameter("username", userName).getResultList();
		if (lstResult != null && lstResult.size() > 0) {
			return lstResult.get(0);
		}
		return null;
	}

	@Override
	public boolean addUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		entityManager.persist(user);
		return true;
	}

	@Override
	public void updateUser(User user) {
		User mUser = entityManager.find(User.class, user.getId());
		user.setPassword(mUser.getPassword());
		entityManager.merge(user);
	}

	@Override
	public void deleteUserByUsername(String userName) {
		User mUser = getUserByUserName(userName);
		mUser.setIsDeleted(1);
		entityManager.merge(mUser);
	}

	@Override
	public boolean userExists(User user) {
		String hql = "FROM User as u WHERE u.isDeleted = 0 AND (u.username = :username OR u.email=:email)";
		return entityManager.createQuery(hql).setParameter("username", user.getUsername()).setParameter("email", user.getEmail()).getResultList().size() > 0;
	}

	@Override
	public void deleteUserById(Integer id) {
		User mUser = entityManager.find(User.class, id);
		mUser.setIsDeleted(1);
		entityManager.merge(mUser);
	}

	@Override
	public boolean changePassword(TokenResetPassDTO tokenResetDTO) {
		String hql = "FROM User as u WHERE u.username = :username AND u.isDeleted=0";
		@SuppressWarnings("unchecked")
		List<User> lstResult = entityManager.createQuery(hql).setParameter("username", tokenResetDTO.getUsername()).getResultList();
		if (lstResult != null && lstResult.size() > 0) {
			User mUser = lstResult.get(0);
			if(passwordEncoder.matches(tokenResetDTO.getOldPass(), mUser.getPassword())) {
				mUser.setPassword(passwordEncoder.encode(tokenResetDTO.getNewPass()));
				entityManager.merge(mUser);
				// send email inform
//				emailHepler.sendMailWithSimpleText(mUser.getEmail(), "Change password Successfully", "You had change new password successfully!");
				return true;
			}
		}
		return false;
	}
}
