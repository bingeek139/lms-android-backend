package com.ptit.lms.androidlms.service;

import com.ptit.lms.androidlms.dto.TokenResetPassDTO;
import com.ptit.lms.androidlms.entity.User;

public interface UserService {
    boolean authenUser(String username, String password);

    public User getUserByUserName(String userName);

    boolean addUser(User user);

    void updateUser(User user);

    void deleteUserByUsername(String user_name);

    void deleteUserById(Integer id);

    boolean changePassword(TokenResetPassDTO tokenResetDTO);
}
