package com.ptit.lms.androidlms.service.impl;

import com.ptit.lms.androidlms.dao.UserDao;
import com.ptit.lms.androidlms.dto.TokenResetPassDTO;
import com.ptit.lms.androidlms.entity.User;
import com.ptit.lms.androidlms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {


	@Autowired
	private UserDao userDao;

	@Override
	public boolean authenUser(String username, String password) {
		// TODO Auto-generated method stub
		return userDao.authenUser(username, password);
	}

	@Override
	public User getUserByUserName(String userName) {
		return userDao.getUserByUserName(userName);
	}

	@Override
	public boolean addUser(User user) {
		if (userDao.userExists(user)) {
			return false;
		} else {
			userDao.addUser(user);
			return true;
		}
	}

	@Override
	public void updateUser(User user) {
		userDao.updateUser(user);
	}

	@Override
	public void deleteUserByUsername(String userName) {
		userDao.deleteUserByUsername(userName);
	}

	@Override
	public void deleteUserById(Integer id) {
		userDao.deleteUserById(id);
	}


	@Override
	public boolean changePassword(TokenResetPassDTO tokenResetDTO) {
		return userDao.changePassword(tokenResetDTO);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.getUserByUserName(username);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
	}

	private List<SimpleGrantedAuthority> getAuthority(User user) {
		return Collections.singletonList(new SimpleGrantedAuthority(user.getRole()));
	}

}